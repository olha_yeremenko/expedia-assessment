# Expedia assessment task

This project contains test for hotels.com

## Problem statement
This assessment consists of writing an automation code that would give us a csv file as an output with the top 10 costliest hotels with price in London that has guest rating >8.0 on the hotels.com website.

*  Start with opening the web browser and navigating to https://uk.hotels.com/
*  Search for hotels in London 3
*  Check-in Date – 3 months from the current date
*  Check-out Date - 3 months from the current date + 2 days
*  On the search result page, sort the hotels in descending price range (high to low).
*  Store the name of the first 10 hotels, which has guest review rating >8.0 , along with their prices, in a csv file
*  Close the browser

##### What you need to provide

*  Data driven testing
*  Handling UI elements failure
*  Handling timeouts
*  Page Object Modeling

##### Example Output

*  For the search criteria mentioned in test scenario, output should look like below in a csv file:

```
Absolute Pleasure Yacht , 1995
Park Lane City Apartments, 1565
Four Seasons Hotel London at Park Lane, 1125
```

## Problem solution

### Prerequisites
*  java 8
*  maven 

### How to run

From the root folder run command in the terminal: 

```
mvn clean test -Dcucumber.options="--tags @Search"
```

To generate report use: 
```
mvn allure:report
```

##### Output
-  Path to test report `expedia/target/site/allure-maven-plugin/index.html`
-  Path to the csv file: `/expedia/target/top10hotels-{timestamp}.csv`


#### Built With
 
*  **Spring** -  Used for bean injection 
*  **Selenide** -  Selenide is a framework for test automation powered by Selenium WebDriver <https://selenide.org/index.html>. It has a lot of in-box solutions as ajax support, simple configuration, shutdown browser mechanism, handled timeouts etc.
*  **Cucumber** - Was used for implementing BDD approach (quite popular nowadays)
*  **JUnit** - Was used to run cucumber tests 
*  **AllureReport** - Report tool 
*  **Maven** -  Build tool

## Solution description 

Selenide library was selected to interact with browser. It’s a wrapper of Selenium and it allows to setup browser configure very quickly and efficient, so driver.exe is no longer needed. 

### Data driven testing
 Cucumber was used to implement DDT, because it allows to create parametrised reusable steps. Also single test can be run with multiple data sets with help of Cucumber DataTables. 

### Handling UI elements failure
- Selenuim allows to handle UI elements failure with Fluent waiter, where user can configure condition, time, pulling time and ignoring of exertion: 

```
new FluentWait<WebDriver>(driver)
  .withTimeout(50, TimeUnit.SECONDS)
  .pollingevery(3, TimeUnit.SECONDS)
  .ignoring(NoSuchElementException.class);
```

- Selenide allows to do the same in one line `element.waitUntil(appears, Time, PullingTime);`. Exceptions are ignored by default. 
With *should* exceptions are not ignored. `element.should(appear);` This mechanism gives a lot of flexibility and suitable for majority of cases. 

### Handling timeouts
- General Implicit wait for WebDriver in Selenide was set as *Configuration.timeout= 10L* in setupBrowser() method
```
public class Step {
  @BeforeClass
  public void setUpBrowser() {
    WebDriverManager.chromedriver().setup();
    Configuration.holdBrowserOpen = true;
    Configuration.startMaximized = true;
    Configuration.timeout= 10L;
  }
}
```
- Addition timeout handing was implemented for specific tasks. For example, to wait until Ajax is completed and some filtering/sorting was applied to the search results - custom method was added.
- Where Selenide waits until  `.processing`  class appears in DOM and disappears for particular element. 

```
public static void waitUntilLoadSpinnerDisappear(final String cssSelector) {
  $(cssSelector.concat(" .processing"))
      .waitUntil(appear, EXTREMELY_LONG_WAIT, PULL_WAIT)
      .waitUntil(disappear, EXTREMELY_LONG_WAIT, PULL_WAIT);
}
```
Fluent waiter is default for methods: shouldBe(condition), waitUntil(condition,totalWait,pullWait) etc. 

### Page Object Modeling

* PageObject pattern was used to build current framework. Classes for storing locators/elements were marked with @PageObject annotation.
* PageFactory patter is not needed, Spring creates one instans of class by default. Sping configuration is  in `com.expedia.oyeremenko.assessment.core.config.BeanConfig`. Selenide autowire WebDriver automatically. 
* Hotels.com has a lot of components as header, footer, search form, filter block, main block with results, search result item (hotel description). In this case **Component Object** patter is more suitable. It reduces code duplication, because components are generic and can be reused on different pages. It makes framework very extendable and “describe” web-page in more readable way.  


### Potential improvements
- Configure logger property 
- Add Selenide screenshots to Allure report, it will help to analyse test failures 
- Parallel test execution 
- Add ability to run tests in different browsers (also headless) 
- Add mvn/spring profiles to run on multiple environments 
- Configure Dockerfile etc
- Add runner to rerun failed tests
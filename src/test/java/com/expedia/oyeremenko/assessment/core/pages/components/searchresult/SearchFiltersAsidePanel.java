package com.expedia.oyeremenko.assessment.core.pages.components.searchresult;

import static com.codeborne.selenide.Selenide.$;

import com.codeborne.selenide.SelenideElement;
import com.expedia.oyeremenko.assessment.core.pages.components.FilterSlider;
import com.expedia.oyeremenko.assessment.core.utils.PageObject;
import org.springframework.beans.factory.annotation.Autowired;

@PageObject
public class SearchFiltersAsidePanel {

  @Autowired
  private FilterSlider sliderGuestRating;

  @Autowired
  private FilterSlider sliderFilterPrice;

  public SelenideElement getFiltersAsidePanel() {
    return $("#slidePanel #filters");
  }

  public FilterSlider getFilterGuestRatingSlider() {
    sliderGuestRating.setItem($("#filter-guest-rating"));
    return sliderGuestRating;
  }

  public FilterSlider getFilterPriceSlider() {
    sliderFilterPrice.setItem($("#filter-price-rating"));
    return sliderFilterPrice;
  }

}

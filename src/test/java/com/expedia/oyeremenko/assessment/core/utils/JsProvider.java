package com.expedia.oyeremenko.assessment.core.utils;

import static com.expedia.oyeremenko.assessment.core.utils.TimeConstants.EXTREMELY_LONG_WAIT;
import static com.expedia.oyeremenko.assessment.core.utils.TimeConstants.PULL_WAIT;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public class JsProvider {

  private JsProvider() {
  }

  public static void clickByJS(SelenideElement elementToClick) {
    Selenide.executeJavaScript("arguments[0].click();", elementToClick.toWebElement());
  }


  public static void waitForLoad() {
    new WebDriverWait(WebDriverRunner.getWebDriver(), EXTREMELY_LONG_WAIT, PULL_WAIT)
        .until((ExpectedCondition<Boolean>) wd ->
            "complete".equals(Selenide.executeJavaScript("return document.readyState")));
  }

}

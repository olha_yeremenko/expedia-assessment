package com.expedia.oyeremenko.assessment.core.pages.components;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.expedia.oyeremenko.assessment.core.utils.PageObject;

@PageObject
public class SearchFormWidget {

  public SelenideElement getParentContainer() {
    return $(".container-queryform");
  }

  public SelenideElement getSearchInput() {
    return getParentContainer().$("#qf-0q-destination");
  }

  public SelenideElement getSearchButton() {
    return getParentContainer().$("form[role='search'] button[type='submit']");
  }

  public SelenideElement getCheckInInput() {
    return getParentContainer().$("#qf-0q-localised-check-in");
  }

  public SelenideElement getCheckOutInput() {
    return getParentContainer().$("#qf-0q-localised-check-out");
  }

  public SelenideElement getDataPickerCloseBtn() {
    return getParentContainer().$("button.widget-overlay-close");
  }

  public ElementsCollection getAutoSuggestDropdown() {
    return $$(".widget-autosuggest-visible div[class=autosuggest-category-result]");
  }
}

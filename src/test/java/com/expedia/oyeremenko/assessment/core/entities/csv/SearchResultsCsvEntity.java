package com.expedia.oyeremenko.assessment.core.entities.csv;

import com.opencsv.bean.CsvBindByPosition;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
@Data
public class SearchResultsCsvEntity {

  @CsvBindByPosition(position = 0)
  private String property;

  @CsvBindByPosition(position = 1)
  private int price;

}

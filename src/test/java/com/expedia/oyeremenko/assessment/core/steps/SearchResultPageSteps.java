package com.expedia.oyeremenko.assessment.core.steps;

import static com.codeborne.selenide.Condition.appears;
import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.withText;
import static com.expedia.oyeremenko.assessment.core.utils.TimeConstants.EXTREMELY_LONG_WAIT;
import static com.expedia.oyeremenko.assessment.core.utils.TimeConstants.LONG_WAIT;
import static com.expedia.oyeremenko.assessment.core.utils.TimeConstants.PULL_WAIT;
import static com.expedia.oyeremenko.assessment.core.utils.TimeConstants.WAIT;
import static io.netty.util.internal.StringUtil.EMPTY_STRING;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.expedia.oyeremenko.assessment.core.entities.csv.SearchResultsCsvEntity;
import com.expedia.oyeremenko.assessment.core.pages.SearchResultPage;
import com.expedia.oyeremenko.assessment.core.pages.components.FilterSlider;
import com.expedia.oyeremenko.assessment.core.steps.helpers.SearchResultHelpers;
import com.expedia.oyeremenko.assessment.core.utils.CsvUtils;
import com.expedia.oyeremenko.assessment.core.utils.WaiterUtils;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

public class SearchResultPageSteps extends Step {

  @Autowired
  private SearchResultPage searchResultPage;

  @Autowired
  private SearchResultHelpers stepHelpers;


  @Then("I should see Search Result page$")
  public void shouldSeeHomePage() {
    searchResultPage.getResultBlock().getItem().waitUntil(visible, EXTREMELY_LONG_WAIT);
  }

  @When("^at Search Result page for Filter Guest Rating I set min '(.*?)'$")
  public void atSearchResultPageForFilterGuestRatingSetMin(final int expectedValue) {
    FilterSlider guestRatingSlider = searchResultPage.getFiltersAsidePanel().getFilterGuestRatingSlider();

    guestRatingSlider.getSliderControlMin().scrollTo();
    stepHelpers.moveSliderControllerMin(expectedValue, guestRatingSlider);

    WaiterUtils.waitUntilLoadSpinnerDisappear("#filter-guest-rating");
  }


  @When("^at Search Result page for Sort By I set Price '(high to low|low to high)'$")
  public void atSearchResultPageSetPriceSort(final String value) {
    searchResultPage.getSortMenu().getPriceFiler().scrollTo();
    searchResultPage.getSortMenu().getPriceFiler().hover().hover().click();
    searchResultPage.getSortMenu().getPriceFilerDropdown().should(visible).find(withText(value)).click();

    WaiterUtils.waitUntilLoadSpinnerDisappear("#tools-menu");
  }

  @When("^at Search Result page I store first '(.*?)' items to the csv$")
  public void atSearchResultPageGetFirstItems(final int size) {
    List<SearchResultsCsvEntity> resultsCsv = new ArrayList<>();
    for (int i = 1; i <= size; i++) {
      waitUtilResultsLoaded();

      SelenideElement resultItem = searchResultPage.getResultBlock()
          .getResultItemByPosition(i)
          .waitUntil(appears, LONG_WAIT, PULL_WAIT);

      resultItem.scrollTo();
      resultItem.waitUntil(visible, LONG_WAIT);

      String name = resultItem.attr("data-title");
      int price = (int) Double.parseDouble(getPriceFromSearchResult(resultItem));

      resultsCsv.add(new SearchResultsCsvEntity(name, price));
    }
    String filePath = new StringBuilder("target/top10hotels-")
        .append(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyddMMHHmmss")))
        .append(".csv").toString();

    CsvUtils.writeCsvFromBean(filePath, resultsCsv);
  }

  private void waitUtilResultsLoaded() {
    SelenideElement loadListings = searchResultPage.getResultBlock().getLoadListings();

    if (loadListings.has(attribute("style", "display: block;")) || loadListings.is(visible)) {
      loadListings.waitUntil(
          attribute("style", "display: none;"),
          EXTREMELY_LONG_WAIT, PULL_WAIT);

      Selenide.sleep(WAIT);
    }
  }

  private String getPriceFromSearchResult(final SelenideElement resultItem) {
    return resultItem.attr("data-info").isEmpty()
        ? EMPTY_STRING
        : resultItem.attr("data-info").substring(resultItem.attr("data-info").lastIndexOf('|') + 1);
  }
}

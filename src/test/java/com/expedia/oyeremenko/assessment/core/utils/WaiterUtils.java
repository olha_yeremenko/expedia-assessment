package com.expedia.oyeremenko.assessment.core.utils;

import static com.codeborne.selenide.Condition.appear;
import static com.codeborne.selenide.Condition.disappear;
import static com.codeborne.selenide.Selenide.$;
import static com.expedia.oyeremenko.assessment.core.utils.TimeConstants.EXTREMELY_LONG_WAIT;
import static com.expedia.oyeremenko.assessment.core.utils.TimeConstants.PULL_WAIT;

public class WaiterUtils {

  public static void waitUntilLoadSpinnerDisappear(final String cssSelector) {
    $(cssSelector.concat(" .processing"))
        .waitUntil(appear, EXTREMELY_LONG_WAIT, PULL_WAIT)
        .waitUntil(disappear, EXTREMELY_LONG_WAIT, PULL_WAIT);
  }

}

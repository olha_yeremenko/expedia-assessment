package com.expedia.oyeremenko.assessment.core.steps;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.open;

import com.codeborne.selenide.SelenideElement;
import com.expedia.oyeremenko.assessment.core.pages.HomePage;
import com.expedia.oyeremenko.assessment.core.utils.JsProvider;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

public class HomePageSteps extends Step {

  @Autowired
  private HomePage homePage;

  @Value("${base.url}")
  private String homeUrl;

  @Given("I open '(.*?)'$")
  public void shouldOpenHomePage(final String url) {
    open(url);
  }

  @Given("I open Home Search page$")
  public void shouldPage() {
    open(homeUrl);
  }

  @When("I close all popups$")
  public void atHomeSearchPageSearchClosePopups() {
    if (homePage.getPolicyBannerAcceptBtn().is(visible)) {
      homePage.getPolicyBannerAcceptBtn().click();
    }
    if (homePage.getPromotionBannerCloseBtn().is(visible)) {
      homePage.getPromotionBannerCloseBtn().click();
    }
  }


  @When("^at Home Search page on the Search Form I enter '(.*?)' in destination input field$")
  public void atHomeSearchPageSearchFormEnterDestination(final String destination) {
    JsProvider.clickByJS(homePage.getSearchFormWidget().getSearchInput().should(visible));
    homePage.getSearchFormWidget().getSearchInput().should(visible).val(destination);
  }

  @When("^at Home Search page on the Search Form I select '(.*?)' from suggestion dropdown")
  public void atHomeSearchPageSearchFormSelectSuggestedDestination(final String suggestedDestination) {
    homePage.getSearchFormWidget().getAutoSuggestDropdown()
        .findBy(text(suggestedDestination)).click();
  }

  @When("^at Home Search page on the Search Form I select check-out date with month '(.*?)' days '(.*?)' from the current date")
  public void atHomeSearchPageSearchFormSelectCheckOutDateWithCondition(final int monthToAdd, final int dayToAdd) {
    setValueToSearchDataInput(
        homePage.getSearchFormWidget().getCheckOutInput(),
        getDatePlusMonthAndDays(monthToAdd, dayToAdd));
  }

  @When("^at Home Search page on the Search Form I select check-in date with month '(.*?)' days '(.*?)' from the current date")
  public void atHomeSearchPageSearchFormSelectCheckInDateWithCondition(final int monthToAdd, final int dayToAdd) {
    setValueToSearchDataInput(
        homePage.getSearchFormWidget().getCheckInInput(),
        getDatePlusMonthAndDays(monthToAdd, dayToAdd));
  }

  @When("^at Home Search page on the Search Form I select date for (check-in|chek-out) as '(.*?)'$")
  public void atHomeSearchPageSearchFormSelectCheckInDate(final String checkInOut, final String date) {
    SelenideElement input;
    if ("check-in".equals(checkInOut)) {
      input = homePage.getSearchFormWidget().getCheckInInput();
    } else {
      input = homePage.getSearchFormWidget().getCheckOutInput();
    }
    setValueToSearchDataInput(input, date);
  }

  @When("^at Home Search page on the Search Form I click Search button$")
  public void atHomeSearchPageSearchFormClickSearchButton() {
    JsProvider.clickByJS(homePage.getSearchFormWidget().getSearchButton());
  }

  private String getDatePlusMonthAndDays(final int monthCount, final int dayCount) {
    LocalDate localDate = LocalDate.now().plusMonths(monthCount).plusDays(dayCount);
    String newDate = localDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    return newDate;
  }

  private void setValueToSearchDataInput(final SelenideElement dateInput, final String inputDate) {
    dateInput.val(inputDate);
    closeDataPickerWidget();
  }

  private void closeDataPickerWidget() {
    if (homePage.getSearchFormWidget().getDataPickerCloseBtn().isDisplayed()) {
      homePage.getSearchFormWidget().getDataPickerCloseBtn().scrollTo();
      JsProvider.clickByJS(homePage.getSearchFormWidget().getDataPickerCloseBtn());
    }
  }
}
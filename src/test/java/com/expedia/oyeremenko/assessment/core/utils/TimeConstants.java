package com.expedia.oyeremenko.assessment.core.utils;

public class TimeConstants {

  public static final int EXTREMELY_LONG_WAIT = 60000;
  public static final int LONG_WAIT = 20000;
  public static final int WAIT = 6000;
  public static final int PULL_WAIT = 100;
  public static final int JS_PULL_WAIT = 500;

}

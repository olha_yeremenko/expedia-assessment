package com.expedia.oyeremenko.assessment.core.steps.helpers;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.expedia.oyeremenko.assessment.core.pages.components.FilterSlider;
import org.springframework.stereotype.Component;

@Component
public class SearchResultHelpers {

  public void moveSliderControllerMin(int expectedValue, FilterSlider slider) {
    SelenideElement controlMin = slider.getSliderControlMin();

    int xPositionNow = Integer.parseInt(controlMin.getAttribute("aria-valuenow"));
    int sliderWidth = slider.getSliderHighlight().getSize().getWidth();
    int offset = (expectedValue - xPositionNow) * sliderWidth / 10;

    Selenide.actions().dragAndDropBy(controlMin, offset, 0).build().perform();
  }

}

package com.expedia.oyeremenko.assessment.core.pages;

import com.expedia.oyeremenko.assessment.core.pages.components.searchresult.SearchFiltersAsidePanel;
import com.expedia.oyeremenko.assessment.core.pages.components.searchresult.SearchResultBlock;
import com.expedia.oyeremenko.assessment.core.pages.components.searchresult.SearchResultSortMenu;
import com.expedia.oyeremenko.assessment.core.utils.PageObject;
import org.springframework.beans.factory.annotation.Autowired;

@PageObject
public class SearchResultPage {

  @Autowired
  private SearchResultBlock resultBlock;

  @Autowired
  private SearchResultSortMenu sortMenu;

  @Autowired
  private SearchFiltersAsidePanel filtersAsidePanel;

  public SearchResultSortMenu getSortMenu() {
    return sortMenu;
  }

  public SearchResultBlock getResultBlock() {
    return resultBlock;
  }

  public SearchFiltersAsidePanel getFiltersAsidePanel() {
    return filtersAsidePanel;
  }

}


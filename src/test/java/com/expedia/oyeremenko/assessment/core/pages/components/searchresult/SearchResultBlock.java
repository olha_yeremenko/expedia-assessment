package com.expedia.oyeremenko.assessment.core.pages.components.searchresult;

import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selenide.$;

import com.codeborne.selenide.SelenideElement;
import com.expedia.oyeremenko.assessment.core.utils.PageObject;

@PageObject
public class SearchResultBlock {

  public SelenideElement getItem() {
    return $(byId("search-results"));
  }

  public SelenideElement getResultList() {
    return $("ol.listings");
  }

  public SelenideElement getResultItemByPosition(final int id) {
    return getResultList().$("li.hotel[data-info^='" + id + "|']");
  }

  public SelenideElement getLoadListings() {
    return $(byId("listings-loading"));
  }
}

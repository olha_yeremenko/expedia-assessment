package com.expedia.oyeremenko.assessment.core.pages.components.searchresult;

import static com.codeborne.selenide.Selenide.$;

import com.codeborne.selenide.SelenideElement;
import com.expedia.oyeremenko.assessment.core.utils.PageObject;

@PageObject
public class SearchResultSortMenu {

  public SelenideElement getItem() {
    return $("#tools-menu");
  }

  public SelenideElement getPriceFiler() {
    return getItem().$("#enhanced-sort a[data-menu='sort-submenu-price']");
  }

  public SelenideElement getPriceFilerDropdown() {
    return getItem().$(".submenu-wrap #sort-submenu-price");
  }
}

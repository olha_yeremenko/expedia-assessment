package com.expedia.oyeremenko.assessment.core.pages.components;

import com.codeborne.selenide.SelenideElement;
import com.expedia.oyeremenko.assessment.core.utils.PageObject;

@PageObject
public class FilterSlider {

  private SelenideElement parentFilterItem;

  public SelenideElement getItem() {
    return parentFilterItem;
  }

  public void setItem(SelenideElement parentFilterItem) {
    this.parentFilterItem = parentFilterItem;
  }

  public SelenideElement getSliderControlMin() {
    return parentFilterItem.$(".widget-slider-handle-min[role='slider']");
  }

  public SelenideElement getSliderControlMax() {
    return parentFilterItem.$(".widget-slider-handle-max[role='slider']");
  }

  public SelenideElement getSliderHighlight() {
    return parentFilterItem.$(".widget-slider-cont");
  }
}

package com.expedia.oyeremenko.assessment.core.pages;

import static com.codeborne.selenide.Selenide.$;

import com.codeborne.selenide.SelenideElement;
import com.expedia.oyeremenko.assessment.core.pages.components.SearchFormWidget;
import com.expedia.oyeremenko.assessment.core.utils.PageObject;
import org.springframework.beans.factory.annotation.Autowired;

@PageObject
public class HomePage {

  @Autowired
  private SearchFormWidget searchFormWidget;

  public SearchFormWidget getSearchFormWidget() {
    return searchFormWidget;
  }

  public SelenideElement getPolicyBannerAcceptBtn() {
    return $("button.cookie-policy-banner-accept");
  }

  public SelenideElement getPromotionBannerCloseBtn() {
    return $("button.widget-overlay-close");
  }
}

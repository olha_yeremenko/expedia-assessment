package com.expedia.oyeremenko.assessment.core.utils;

import static com.opencsv.CSVWriter.DEFAULT_SEPARATOR;

import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CsvUtils {

  /**
   * @param objectsToWrite object should use some mapping strategy
   */
  public static void writeCsvFromBean(final String path, final List<?> objectsToWrite) {
    try {
      Writer writer = new FileWriter(path);
      StatefulBeanToCsv sbc = new StatefulBeanToCsvBuilder(writer)
          .withSeparator(DEFAULT_SEPARATOR)
          .build();

      sbc.write(objectsToWrite);
      writer.close();
    } catch (IOException | CsvDataTypeMismatchException | CsvRequiredFieldEmptyException e) {
      log.error("Failed to write csv file '{}' : {}", path, e.getMessage());
    }
    log.info("CSV file '{}' was created", path);
  }
}

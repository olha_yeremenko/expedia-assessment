package com.expedia.oyeremenko.assessment.core.steps;

import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

import com.codeborne.selenide.Configuration;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.AfterClass;
import org.junit.BeforeClass;

public class Step {

  @BeforeClass
  public void setUpBrowser() {
    WebDriverManager.chromedriver().setup();
    Configuration.holdBrowserOpen = true;
    Configuration.startMaximized = true;
    Configuration.timeout= 10L;
  }

  @AfterClass
  public void tearDownBrowser() {
    getWebDriver().manage().deleteAllCookies();
    getWebDriver().close();
    getWebDriver().quit();
  }
}
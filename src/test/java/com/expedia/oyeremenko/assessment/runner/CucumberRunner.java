package com.expedia.oyeremenko.assessment.runner;


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import lombok.extern.slf4j.Slf4j;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = "classpath:features",
    plugin = {"pretty",
        "json:target/cucumber/cucumber.json",
        "junit:target/cucumber/cucumber.xml",
        "html:target/cucumber-html",
        "io.qameta.allure.cucumber4jvm.AllureCucumber4Jvm",
        "rerun:target/cucumber/rerun.txt"},
    glue = {"com.expedia.oyeremenko.assessment"},
    monochrome = true)
@ContextConfiguration("classpath:cucumber.xml")
@Slf4j
public class CucumberRunner {

}


# Created by olha.yeremenko at 26/02/2020
Feature: Search for Hotel. Save first 10 to csv

  @Search
  Scenario: Search for a hotel in London
    Given I open Home Search page
    When  I close all popups
    When at Home Search page on the Search Form I enter 'London' in destination input field
    When at Home Search page on the Search Form I select 'London, United Kingdom' from suggestion dropdown
    When at Home Search page on the Search Form I select check-in date with month '3' days '0' from the current date
    When at Home Search page on the Search Form I select check-out date with month '3' days '2' from the current date
    When at Home Search page on the Search Form I click Search button
    Then I should see Search Result page
    When at Search Result page for Filter Guest Rating I set min '8'
    When at Search Result page for Sort By I set Price 'high to low'
    Then at Search Result page I store first '10' items to the csv